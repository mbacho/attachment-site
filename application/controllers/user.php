<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	public function signin($error_msg = False)
	{
		if($this->session->userdata('logged_in')){
			redirect(base_url('home'));
			return;
		}

		$this->load->library('form_validation');
		$data = array();
		$data['error_msg'] = $error_msg;
		$this->load->view('signin', $data);
	}

	public function signin_submit()
	{
		if($this->session->userdata('logged_in')){
			redirect(base_url('home'));
			return;
		}

		// validate form
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === False){
			$this->signin();
			return;
		}

		// authenticate user
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->user_model->authenticate($username, $password);
		if($result === False){
			$this->signin(True);
			return;
		}

		// create user session
		$userdata = array();
		$userdata['username'] = $result->username;
		$userdata['name'] = $result->name;
		$userdata['role'] = $result->role;
		$userdata['logged_in'] = TRUE;

		$this->session->set_userdata($userdata);
		
		redirect(base_url('home'));
	}

	public function home()
	{
		if($this->session->userdata('logged_in') === False){
			// maybe redirect to landing page
			redirect(base_url('signin'));
			return;
		}

		$this->load->view('home');
	}

	public function signout(){
		if($this->session->userdata('logged_in')){
			$this->session->sess_destroy();
		}

		// maybe redirect to landing page
		redirect(base_url('signin'));
	}
}