<?php

class User_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function authenticate($username, $password)
	{
		$query = $this->db->get_where('user', array('username'=> $username));
		
		// check if user exists

		if($query->num_rows() < 1 ){
			return False;
		}

		$row = $query->row();

		// check if password is correct
		if($row->password !== $password){
			return False;
		}

		return $row;
	}
}