<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign In - Attachment Site</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<div class="container">
		<div class="span4 offset4">
			<form action="<?php echo base_url('signin_submit'); ?>" class="form well" method="POST">
				<fieldset>
					<legend>Please sign in</legend>
					<?php if($error_msg) { ?>
						<p>Sorry, your username or password is incorrect.</p>
					<?php }?>
					<div class="control-group">
						<label for="username" class="control-label">Username</label>
						<div class="controls">
							<input type="text" name="username" value="<?php echo set_value('username','');?>">
							<?php echo form_error('username');?>
						</div>
					</div>
					<div class="control-group">
						<label for="password" class="control-label">Password</label>
						<div class="controls">
							<input type="password" name="password" value="<?php echo set_value('password','');?>">
							<?php echo form_error('password');?>
						</div>
					</div>
					<button class="btn" type="submit">Sign In</button>
				</fieldset>
			</form>
		</div>
	</div>
</body>
</html>