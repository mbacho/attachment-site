<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home - Attachment Site</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<div class="container">
		<h1>Welcome <?php echo $this->session->userdata('name'); ?></h1>
		<a href="<?php echo base_url('signout'); ?>">Sign Out</a>
		<p>Role: <?php echo $this->session->userdata('role'); ?></p>
	</div>
</body>
</html>