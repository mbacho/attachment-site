-- Weka schema hapa

DROP TABLE IF EXISTS user;

CREATE TABLE user(
	id int(11) NOT NULL AUTO_INCREMENT,
	username varchar(200) NOT NULL, -- username for others, reg no for students
	email varchar(200) NOT NULL,
	password varchar(200) NOT NULL,
	role varchar(200) NOT NULL,
	name varchar(200), -- how the user is displayed in the site, not the username
	PRIMARY KEY(id)
);

INSERT INTO user VALUES('','ac','ac@uonbi.ac.ke','ac','ac','Attachment Coordinator');